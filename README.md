## Descrição da vaga

Estamos em busca de devs para fazer parte do nosso time imediatamente! A todo gás em um novo projeto e queremos você na equipe. Trabalhamos em um clima informal com foco em produtividade das tarefas, que em alguns casos são recompensadas com bônus $$$.

## Local

Escritório 
01243-002, Consolação - São Paulo, SP

## Requisitos

**Obrigatórios:**

- Inglês intermediário
- PHP, MySQL e estruturação de banco de dados
- Conhecimentos em Laravel e/ou outro MVC 
- Noção de uso de Interfaces e Traits
- Ter paciência de documentar o próprio código
- Proatividade para o planejamento antes de executar a tarefa
- Boa vontade de compartilhar o que sabe

**Desejáveis:**

- Saber trabalhar com uso de boas práticas
- Conhecimento de Node.js
- Consumo e criação de API Rest

**Diferenciais:**

- Ensino superior completo
- Possuir projetos pessoais

## Contratação

PJ a combinar

## Nossa empresa

Somos uma agência que atua em São Paulo e Rio de Janeiro. Trabalhamos com desenvolvimento de soluções ecommerce customizadas, marketing digital e também treinamentos.

## Como se candidatar

Por favor envie um email para contato@otimize.me com assunto: Vaga Backend.
Anexe seu CV e opcionalmente, envie-nos seu perfil em sites que lhe apresentem(ex: trampos) ou propriamente seu perfil no GitHub.

## Labels

- Alocado
- PJ
- Pleno
- Remoto - Parcial
- Sênior